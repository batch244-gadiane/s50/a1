//import{Fragment} from 'react';
import{Container} from 'react-bootstrap';
import {BrowserRouter as Router,Routes, Route} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavbar from './components/AppNavbar';
import './App.css';
import Home from './pages/Home';
import Course from './pages/Course';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import {useState,useEffect} from 'react';
import { UserProvider} from './UserContext';
import CourseView from './pages/CourseView';

function App() {

  //Global state hook for the user information for validating if a user is logged in 
  const [user, setUser] = useState ({
    id: null,
    isAdmin: null
  });

  //Function  for clearing localStorage on logout

    const unsetUser = () => {
        localStorage.clear();
    }

    useEffect (() => {
      console.log(user);
      console.log(localStorage);

    },[user]) 


  return (
  //fragment is container or wrapper for the components 
// In REact JS, multiple components rendered in single component should wrapped in a parent componennt
    //<fragment></fragment> or <></>  ensures that error will be prevented

    //we store information in the context by providing the infomration using the UserProvider comonent and passing the information via value prop
    //All information inside the value prop will bw accessible to pages/components wrapped the UserProvider.

    <UserProvider value ={{user,setUser, unsetUser}}>
       <Router>
          <AppNavbar/>
            <Container>
              <Routes>
                <Route path ="/" element ={<Home/>}/>
                <Route path ="/course" element ={<Course/>}/>
                 <Route path ="/courses/:courseId" element ={<CourseView/>}/>
                <Route path ="/login" element ={<Login/>}/>
                <Route path ="/register" element ={<Register/>}/>
                <Route path ="/logout" element ={<Logout/>}/>
                <Route path="*" element={<Error/>} /> 
            </Routes>
         </Container>
        </Router>

    </UserProvider>


   );
}  

export default App;
