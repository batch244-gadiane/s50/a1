//we will use react's context API to give the logged in user the gloval scope within our aplication

import React from 'react';

const UserContext = React.createContext();


// The "Provider" component allows other components to consume/use the context object and supply the necessary information needed to the context object
 console.log(UserContext);
export const UserProvider =  UserContext.Provider;


export default UserContext;