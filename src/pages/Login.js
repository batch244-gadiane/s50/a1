import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';



export default function Login (){

	const {user,setUser} = useContext(UserContext);


	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const[isActive, setActive] = useState(false);



function autheticate(e){

	e.preventDefault();

	// Process a fetch request from the correspondinf backend API
        // The header information "Content-type" is used to specifiy that the info being sent to the backend will be sent in the form JSON
        // The fetch request will communicate with our BE application providing it with a stringified JSON
        // Convert the info into JS object using .then => (res.json())
	// Syntax:
            /*
                fetch('url', {options})
                .then(res => res.json())
                .then(data => {})
            
            */

	fetch('http://localhost:4000/users/login',{
		method: 'POST',
		headers:{
			'Content-Type' : 'application/json'
		},
			body: JSON.stringify({
				email: email,
				password: password
			})
	})
	.then(res => res.json())
	.then(data =>{
		console.log(data)

		if(data.access !== undefined){
			localStorage.setItem('token',data.access);
			retrieveUserDetails(data.access);

			Swal.fire({
				title: "Login Successful",
				icon: "success",
				text: "Welcome to Zuitt!"
			})

		}else {
			Swal.fire({
				title: "Authentication failed!",
				icon: "error",
				text: "Check you details!"
			})
		}

	})

	
	//localStorage.setItem('email', email);
	// Set the global user state to have properties obtained from local storage
	 // Though access to the user information can be done via the localStorage this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout
	// When states change components are rerendered and the AppNavbar component will be updated based on the user credentials, unlike when using the localStorage where the localStorage does not trigger component rerendering

	//setUser({email: localStorage.getItem('email')});

	setEmail('');
	setPassword('');
	
	//alert("You are now logged in!");

}

	const retrieveUserDetails = (token) =>{
		fetch('http://localhost:4000/users/details', {
			headers : {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			setUser({
				id:data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		if(email !=='' && password !=='')
		{
			setActive(true);
		}else {
			setActive(false);
		}


	},[email, password]);



return(

		//Conditional rendering that will
		(user.id !== null)
		?
			<Navigate to='/course'/>
		:
//Invokes the autheticate fuction upon clickinh on submit nbutton

		<Form onSubmit={(e) => autheticate(e)}>
		      <Form.Group className="mb-3" controlId="logInEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control type="email" 
		        placeholder="Enter email"
		        required
		        value={email}
				onChange={e => setEmail(e.target.value)} />
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="logInPassword">
		        <Form.Label>Password</Form.Label>
		        <Form.Control type="password" placeholder="Password"
		        required
		        value={password}
				onChange={e => setPassword(e.target.value)} />
		      </Form.Group>
		     

		 	{ isActive
			 ?
			 <Button variant ="primary" type ="submit" id="submitbtn">Submit</Button>
			 :
			 <Button variant ="danger" type ="submit" id="submitbtn " disabled>Submit</Button>
			 
			}
			
		</Form>

	)


}