import{Form, Button}  from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import{Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';



export default function Register(){
	
	const {user} = useContext(UserContext);

	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2]= useState('');
	const[isActive, setActive] = useState(false);
	const [firstName, setFirstName]= useState('');
	const [lastName, setLastName]= useState('');
	const [mobileNo, setMobileNo]= useState('');



	console.log(email);
	console.log(password1);
	console.log(password2);



	//function to simulate user registration

	function registerUser(e) {

		// Prevents the page reloading
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === true) {

				Swal.fire({
					title: "Duplicate email found!",
					icon: "error",
					text: "Kindly provide another email to complete the registration."
				})

			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data === true){
						Swal.fire({
							title: "Registration Successful!",
							icon: "success",
							text: "Welcome to Zuitt!"
						})

						// Clear input fields
						setFirstName('');
						setLastName('');
						setMobileNo('');
						setEmail('');
						setPassword1('');
						setPassword2('');

					} else {
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
		})	

		// alert('Thank you for registering!');
	}

	useEffect(() => {
		if((email !=='' && password1 !=='' & password2 !== '' && firstName !=='' && lastName !=='' & mobileNo.length === 11) && (password1 === password2)){
			setActive(true);
		}else {
			setActive(false);
		}
	},[email, password1, password2, firstName, lastName,mobileNo]);

	return(
		(user.id !== null)
		? //if
			<Navigate to="/course"/>
		: //or


		<Form onSubmit={(e) => registerUser(e)}>
		<Form.Group controlId="lirstName">
		<Form.Label>First Name</Form.Label>
		<Form.Control
			type="text"
			placeholder="Enter First Name"
			required
			value={firstName}
			onChange={e => setFirstName(e.target.value)}

			/>
		</Form.Group>
		<Form.Group controlId="lastName">
		<Form.Label>Last Name</Form.Label>
		<Form.Control
			type="text"
			placeholder="Enter Last Name"
			required
			value={lastName}
			onChange={e => setLastName(e.target.value)}

			/>
		</Form.Group>
		


			<Form.Group controlId="userEmail">
			<Form.Label>Email Address</Form.Label>
			<Form.Control
				type="email"
				placeholder="Enter email"
				required
				value={email}
				onChange={e => setEmail(e.target.value)}

				/>

			<Form.Text className ="text-muted">We'll never share your Email with anyone else.</Form.Text>

			<Form.Group controlId="contactNumber">
			<Form.Label>Mobile Number</Form.Label>
			<Form.Control
				type= "Number"			
				placeholder="Enter Contact Number"
				required
				value={mobileNo}
				onChange={e => setMobileNo(e.target.value)}

				/>
			</Form.Group>

			</Form.Group>

			<Form.Group controlId="password1">
			<Form.Label>Password</Form.Label>
			<Form.Control
				type="password"
				placeholder="Password"
				required
				value={password1}
				onChange={e => setPassword1(e.target.value)}

				/>

			</Form.Group>

			<Form.Group controlId="password2">
			<Form.Label>Verify Password</Form.Label>
			<Form.Control

				type="password"
				placeholder="Verify Password"
				required
				value={password2}
				onChange={e => setPassword2(e.target.value)}

				/>
			

			</Form.Group>
{/*conditional render submit button based on isActive state*/}
 {/*"? and : " is like if else ?=true :=false*/}
			{ isActive
			 ?
			 <Button variant ="primary" type ="submit" id="submitbtn">Submit</Button>
			 :
			 <Button variant ="danger" type ="submit" id="submitbtn " disabled>Submit</Button>
			 
			}
			
		</Form>


		)
}