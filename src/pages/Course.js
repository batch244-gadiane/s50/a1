//import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';
import {useEffect,useState} from 'react';


export default function Courses() {

		//Retrievs the courses
	const [courses, setCourses ] = useState([])

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/courses`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data.map(course => {
				return(
				<CourseCard key={course._id} courseProp = {course}/>)
			}))
		})
	},[])

	// The "map" method loops through the individual course objects in our array and returns a CourseCard component for each course

	//Multiple components created through the map method must have a unique key that will help React JS identify which components/elements have been changed, added, removes
	//key = unique element
	//Evrytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our Course using the courseProp

	/*const courses = coursesData.map(course => {
		return	(
			<CourseCard key={course.id} courseProp = {course}/>
		)
	})
*/
	return (
		<>
			<h1>Courses</h1>
			{courses}
			
		</>

	)
}